Presentation outline:
    What is QML? (history of Qt; explain QML/QtQuick)
    What makes QML so great? (declarative; easy to learn; Qt runs almost anywhere)
    What can you do with QML?
    Examples:
        Basic Elements[Item, Rect, Component, etc.], Properties, Scope, Binding, Signals/Handlers, Anchors,
            States, Transitions, Animation, Timers, ListView, Particles, OpenGL, Threading,
            C++ Communication/Extension          
    Improvements in QML 2.0