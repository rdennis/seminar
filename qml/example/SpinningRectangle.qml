// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: root
    width: 100; height: width
    color: rotationAnimation.paused ? "red" : "green"
    radius: rotationAnimation.paused ? width / 2 : 0
    smooth: true
    property alias paused: rotationAnimation.paused

    function pausePlay() {
        if(rotationAnimation.paused)
            rotationAnimation.resume()
        else
            rotationAnimation.pause()
    }

    border { color: "black"; width: 2 }

    Behavior on color {
        ColorAnimation { duration: 200 }
    }

    Behavior on radius {
        NumberAnimation { duration: 200 }
    }

    RotationAnimation {
        id: rotationAnimation
        target: root
        from: 0; to: 360
        duration: 2000
        running: true
        loops: Animation.Infinite
    }

//    states: [
//        State {
//            name: "PLAY"
//            when: !rotationAnimation.paused
//            PropertyChanges { target: root; color: "green" }
//        },
//        State {
//            name: "PAUSE"
//            when: rotationAnimation.paused
//            PropertyChanges { target: root; color: "red" }
//        }
//    ]
}
