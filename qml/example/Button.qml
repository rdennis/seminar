import QtQuick 1.1

Rectangle {
    width: 100; height: 40
    color: "#d0f0ff"

    border { color: "black" }
    radius: 5

    property alias label: text.text
    signal clicked

    Text {
        id: text
        anchors.centerIn: parent
        text: "Press me!"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: parent.clicked()
    }
}
