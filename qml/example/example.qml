import QtQuick 1.1

Item {
    id: root
    anchors.fill: parent
    focus: true

    SpinningRectangle {
        id: spinningRect
        x: parent.width / 2 - width / 2
        y: parent.height / 2 - height / 2

        property real moveDuration: 1500

        function moveTo(x, y) {
            this.x = x - width / 2
            this.y = y - height / 2
        }

        Behavior on x {
            NumberAnimation { easing.type: Easing.OutBack; duration: spinningRect.moveDuration }
        }
        Behavior on y {
            NumberAnimation { easing.type: Easing.OutBack; duration: spinningRect.moveDuration }
        }
    }

    Keys.onSpacePressed: button.clicked()

    Button {
        id: button
        anchors { bottom: parent.bottom; bottomMargin: 10; horizontalCenter: parent.horizontalCenter }
        label: spinningRect.paused ? "Resume" : "Pause"
        onClicked: spinningRect.pausePlay()
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        z: -1
        drag.target: spinningRect
        onClicked: spinningRect.moveTo(mouseX, mouseY)
    }
}
