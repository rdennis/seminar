import QtQuick 2.0
import Qt.labs.particles 2.0

Item {
    anchors.fill: parent
    z: parent.faded ? 1001 : 0
    ParticleSystem{ id: sys }
    ColoredParticle{
        system: sys
        id: cp
        image: "content/particle.png"
        colorVariation: 0.1
        color: "#fffaaffa"
        additive: 1
    }
    TrailEmitter{
        id: burster
        system: sys
        emitting: false
        particlesPerSecond: 1500
        particleDuration: 500
        acceleration: AngleVector{ angle: 0; angleVariation: 360; magnitude: 1500; }
        particleSize: 4
        particleEndSize: 0
        particleSizeVariation: 2
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            burster.x = mouseX; burster.y = mouseY
            burster.burst(0.1)
            mouse.accepted = false
        }
    }
}
