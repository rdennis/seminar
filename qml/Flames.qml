// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
import Qt.labs.particles 2.0

Item {
    anchors.fill: parent

    ParticleSystem { id: sys }
    ColoredParticle{
        id: flame
        anchors.fill: parent
        system: sys
        particles: ["A"]
        image: "content/particle.png"
        colorVariation: 0.1
        color: "#ff400f"
        additive: 1
    }
    TrailEmitter{
        id: fire
        system: sys
        particle: "A"

        y: parent.height
        width: parent.width

        particlesPerSecond: 1500
        particleDuration: 4000

        acceleration: PointVector{ y: -17; xVariation: 3 }
        speed: PointVector{ xVariation: 3 }

        particleSize: 24
        particleSizeVariation: 8
        particleEndSize: 4
    }
}
