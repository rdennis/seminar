import QtQuick 2.0

Text {
    font.pixelSize: parent.fontSize / 2
    anchors { bottom: parent.bottom; right: parent.right; bottomMargin: -25; rightMargin: 10 }
    color: parent.slideTextColor
}
