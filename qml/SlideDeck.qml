import QtQuick 2.0
import Qt.labs.particles 2.0

CustomTransitionPresentation
{
    id: root
    width: 1024
    height: 768

    property int contentPadding: 10


    Slide {
        title: "QML 2.0"
        centeredText: "Robert Dennis"
    }

    Slide {
        title: "Outline"
        content: [
            "What is QML?",
            "Advantages and Disadvantages",
            "QML basics",
            " Properties, Binding, Anchor Layout, Signals/Handlers",
            " States, Transitions, Animation, Scope",
            "Examples",
            "Acknowledgements"
        ]
    }

    Slide {
        title: "What is QML?"
        content: [
            "Qt",
            " object-oriented cross-platform GUI framework",
            " first released in 1995",
            " developed by Haavard Nord and Eirik Chambe-Eng",
            "Qt Meta-Object Language",
            " released with Qt 4.7 (2009)",
            " CSS-like syntax, JavaScript",
            "Qt Quick",
            " QML, QML runtime, Qt Creator"
        ]
        Footnote {
            text: "Blanchette, Summerfield"
        }
    }

    Slide {
        title: "Advantages"
        content: [
            "Declarative",
            "Easy to learn",
            "Rapid development",
            "Runs anywhere Qt does (a lot of platforms)",
            "Fully customizable",
            "2.0 uses OpenGL"
        ]
    }

    Slide {
        title: "Disadvantages"
        content: [
            "Still in development",
            "No built-in components (qt-components project working on it)",
            "No support for native looking apps",
            "Some performance problems"
        ]
    }

    Slide {
        id: basicsSlide
        title: "Basic Elements"

        content:[
            "import",
            "Item",
            "Rectangle",
            "id"
        ]

        contentWidth: width - basicCS.width - contentPadding

        CodeSection {
            id: basicCS
            text: "
<span style='color: goldenrod'>import</span> QtQuick 2.0<br/><br/>
<span style='color: purple'>Rectangle</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: rect<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>color</span>: <span style='color: green'>\"steelblue\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>width</span>: 150; <span style='color: red'>height</span>: width<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>x</span>: parent.width / 2 - width<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>y</span>: parent.height - height<br/>
}<br/><br/>
<span style='color: purple'>Rectangle</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: circle<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>color</span>: <span style='color: green'>\"lightgreen\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>width</span>: 150; <span style='color: red'>height</span>: width<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>x</span>: parent.width / 2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>y</span>: parent.height - height<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>radius</span>: width / 2<br/>
}"

            Rectangle {
                id: rect
                property int index: 0
                color: "steelblue"
                width: 150; height: width
                x: parent.width / 2 - width
                y: parent.height - height
                opacity: 0
                Behavior on opacity {
                    PropertyAnimation { duration: 500 }
                }
            }
            Rectangle {
                id: circle
                color: "lightgreen"
                width: 150; height: width
                x: parent.width / 2
                y: parent.height - height
                radius: width / 2
                opacity: rect.opacity
                Behavior on opacity {
                    PropertyAnimation { duration: 500 }
                }
            }
        }

        MouseArea {
            anchors.fill: basicCS
            onClicked:{
                if(++rect.opacity > 1)
                    rect.opacity = 0
                if(++circle.opacity > 1)
                    circle.opacity = 0
            }
        }
    }

    Slide {
        title: "Properties"
        content:[
            "Properties have enforced types: int, color, etc.",
            "Properties are defined as:",
            " [default] property <type> <name>[: defaultValue]",
            "alias",
            "Grouped properties",
            " <group> {<property: value>, <property: value>}",
            " <group>.<property: value>"
        ]

        Footnote {
            text: "http://qt-project.org/doc/qt-4.8/qdeclarativebasictypes.html"
        }
    }

    Slide {
        id: bindingSlide
        title: "Binding"
        content: [
            "Properties can be bound to other properties, or JavaScript expressions",
            "Properties are bound when they are declared, or by use of the Binding element",
            "Assingment(=) is not binding"
        ]

        Footnote {
            text: "http://doc.qt.nokia.com/4.7-snapshot/propertybinding.html"
        }
    }

    Slide {
        id: bindingExample
        title: "Binding"
        Item {
            width: parent.width - bindingCS.width - contentPadding; height: parent.height
            Rectangle {
                id: inputBox
                property color selectedColor: input.text
                width: parent.width; height: input.height + input.y * 2
                anchors { top: parent.top; horizontalCenter: parent.horizontalCenter }
                border { color: "black" }

                TextInput {
                    id: input
                    x: 10; y: 10
                    width: parent.width - x * 2
                    font.pixelSize: bindingExample.fontSize
                    text: "transparent"
                }
            }

            Rectangle {
                id: colorRect
                width: parent.width
                anchors { top: inputBox.bottom; bottom: parent.bottom; horizontalCenter: parent.horizontalCenter }
                border { color: "black" }
                color: inputBox.selectedColor

                Behavior on color { ColorAnimation { duration: 750 } }
            }

            MouseArea {
                anchors.fill: parent
                z: input.z - 1
            }
        }

        CodeSection {
            id: bindingCS
            text: "
<span style='color: purple'>Item</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Rectangle</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: inputBox<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>TextInput</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: input<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>x</span>: 10; <span style='color: red'>y</span>: 10<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>width</span>: parent.width - x * 2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>text</span>: <span style='color: green'>\"transparent\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Rectangle</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: colorRect<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>border</span> { <span style='color: red'>color</span>: <span style='color: green'>\"black\"</span> }<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>color</span>: input.text<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Behavior</span> <span style='color: goldenrod'>on</span> <span style='color: red'>color</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>ColorAnimation</span>{<span style='color: red'>duration</span>: 750}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
}"
        }
    }

    Slide {
        id: anchorSlide
        title: "Anchor Layout"
        contentWidth: width / 2
        content: [
            "Anchors stick elements together and let them resize to remain anchored"
        ]

        Rectangle {
            anchors.bottom: parent.bottom
            width: parent.contentWidth; height: childrenRect.height + 10
            radius: 5

            Image {
                id: anchorsImage
                source: "content/anchors.png"
                anchors.left: parent.left
                anchors.leftMargin: 8
                y: 8
            }

            Footnote {
                color: anchorSlide.slideTextColor
                text: "http://qt-project.org/doc/qt-4.8/qml-anchor-layout.html"
                font.pixelSize: 15
            }
        }

        Rectangle {
            id: anchorRect1
            color: "lightgreen"
            width: 200; height: 200
            x: parent.width - width * 1.5
            y: parent.height / 2 - height
            Text {
                anchors.centerIn: parent
                text: "Rect 1"
                font.pointSize: 25
            }
        }
        Rectangle {
            id:anchorRect2
            color: "lightblue"
            width: 200; height: 200
            anchors { top: anchorRect1.bottom; left: anchorRect1.left; right: anchorRect1.right }
            Text {
                anchors.centerIn: parent
                text: "Rect 2"
                font.pointSize: 25
            }
        }

        SequentialAnimation {
            running: visible
            loops: Animation.Infinite
            PropertyAnimation {
                target: anchorRect1
                property: "width"
                to: 300
                duration: 2000; easing.type: Easing.InOutQuad
            }
            PropertyAnimation {
                target: anchorRect1
                property: "width"
                to: 200
                duration: 2000; easing.type: Easing.InOutQuad
            }
        }
    }

    Slide {
        title: "Signals/Handlers"
        contentWidth: width - signalHandlerCS.width - contentPadding
        content: [
            "Signals and Handlers(Slots)",
            " allow for communication with C++",
            " handlers are generated automatically"
        ]

        CodeSection {
            id: signalHandlerCS
            text: "
<span style='color: goldenrod'>signal</span> <span style='color: red'>__mySignal</span><br/>
<span style='color: goldenrod'>signal</span> <span style='color: red'>otherSignal(string arg)</span><br/><br/>
<span style='color: green'>// more code</span><br/><br/>
<span style='color: red'>on__mySignal</span>: {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: blue'>console</span>.log(<span style='color: green'>\"Got signal with<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no args\"</span>)<br/>
}<br/>
<span style='color: red'>onOtherSignal</span>: {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: blue'>console</span>.log(<span style='color: green'>\"Got signal with: \"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> + <span style='color: blue'>arg</span>)<br/>
}<br/>
<br/>
<span style='color: green'>// C++</span><br/>
connect(qmlObject*, <br/>
&nbsp;&nbsp;&nbsp;&nbsp;SIGNAL(signalName(type)),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;QObject*, SLOT(Slot(type)));"
        }
    }

    Slide {
        id: stateSlide
        title: "States"
        contentWidth: width - stateCS.width - contentPadding
        content: [
            "States contain different values for properties",
            "Changes can be made using different Change elements",
            " PropertyChanges, StateChangeScript, AnchorChanges"
        ]

        CodeSection {
            id: stateCS
            text: "
<span style='color: red'>states</span>: [<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>State</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>name</span>: <span style='color: green'>\"GREEN\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>PropertyChanges</span> { <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>target</span>: yellowLight<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>opacity</span>: 0.2 }<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>PropertyChanges</span> { <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>target</span>: redLight<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style='color: red'>opacity</span>: 0.2 }<br/>
&nbsp;&nbsp;&nbsp;&nbsp;},<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>State</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>name</span>: <span style='color: green'>\"YELLOW\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>PropertyChanges</span> { <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>target</span>: greenLight<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>opacity</span>: 0.2 }<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>PropertyChanges</span> { <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>target</span>: redLight<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>opacity</span>: 0.2 }<br/>
&nbsp;&nbsp;&nbsp;&nbsp;},<br/>
&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
]"
        }

        Rectangle {
            id: signalHousing
            x: 150; y: 350
            rotation: -90
            width: 100; height: 250
            color: "black"
            Rectangle {
                id: greenLight
                color: "green"
                width: height; height: parent.height / 3.9
                anchors.bottom: yellowLight.top
                anchors.horizontalCenter: parent.horizontalCenter
                radius: height / 2
            }
            Rectangle {
                id: yellowLight
                color: "yellow"
                width: height; height: parent.height / 3.9
                anchors.centerIn: parent
                radius: height / 2
            }
            Rectangle {
                id: redLight
                color: "red"
                width: height; height: parent.height / 3.9
                anchors.top: yellowLight.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                radius: height / 2
            }

            state: "GREEN"

            states: [
                State {
                    name: "GREEN"
                    PropertyChanges { target: yellowLight; opacity: 0.2 }
                    PropertyChanges { target: redLight; opacity: 0.2 }
                },
                State {
                    name: "YELLOW"
                    PropertyChanges { target: greenLight; opacity: 0.2 }
                    PropertyChanges { target: redLight; opacity: 0.2 }
                },
                State {
                    name: "RED"
                    PropertyChanges { target: greenLight; opacity: 0.2 }
                    PropertyChanges { target: yellowLight; opacity: 0.2 }
                }
            ]

            Timer {
                interval: 750
                running: visible; repeat: true
                onTriggered: {
                    switch(signalHousing.state) {
                    case "GREEN":
                        signalHousing.state = "YELLOW"
                        break
                    case "YELLOW":
                        signalHousing.state = "RED"
                        break
                    case "RED":
                        signalHousing.state = "GREEN"
                        break
                    }
                }
            }
        }

        Footnote {
            text: "http://doc.qt.nokia.com/4.7-snapshot/qdeclarativestates.html"
        }
    }

    Slide {
        title: "Transitions"
        contentWidth: width - transitionCS.width - contentPadding
        content: [
            "Transitions can be used to animate moving between states",
            "Transitions can be done with many different Animation elements",
            " SequentialAnimation, ParallelAnimation, Behavior"
        ]
        CodeSection {
            id: transitionCS
            text: "
<span style='color: red'>transitions</span>: [<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Transition</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>from</span>: <span style='color: green'>\"GREEN\"</span>; <span style='color: red'>to</span>: <span style='color: green'>\"YELLOW\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>NumberAnimation</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>targets</span>: [greenLight, <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yellowLight]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>properties</span>: <span style='color: green'>\"opacity\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>duration</span>: 500<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;},<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Transition</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>from</span>: <span style='color: green'>\"YELLOW\"</span>; <span style='color: red'>to</span>: <span style='color: green'>\"RED\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>NumberAnimation</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>targets</span>: [yellowLight,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;redLight]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>properties</span>: <span style='color: green'>\"opacity\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>duration</span>: 500<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;},<br/>
&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
]"
        }

        Rectangle {
            id: signalHousing1
            x: 150; y: 350
            rotation: -90
            width: 100; height: 250
            color: "black"
            Rectangle {
                id: greenLight1
                color: "green"
                width: height; height: parent.height / 3.9
                anchors.bottom: yellowLight1.top
                anchors.horizontalCenter: parent.horizontalCenter
                radius: height / 2
            }
            Rectangle {
                id: yellowLight1
                color: "yellow"
                width: height; height: parent.height / 3.9
                anchors.centerIn: parent
                radius: height / 2
            }
            Rectangle {
                id: redLight1
                color: "red"
                width: height; height: parent.height / 3.9
                anchors.top: yellowLight1.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                radius: height / 2
            }

            state: "GREEN"

            states: [
                State {
                    name: "GREEN"
                    PropertyChanges { target: yellowLight1; opacity: 0.2 }
                    PropertyChanges { target: redLight1; opacity: 0.2 }
                },
                State {
                    name: "YELLOW"
                    PropertyChanges { target: greenLight1; opacity: 0.2 }
                    PropertyChanges { target: redLight1; opacity: 0.2 }
                },
                State {
                    name: "RED"
                    PropertyChanges { target: greenLight1; opacity: 0.2 }
                    PropertyChanges { target: yellowLight1; opacity: 0.2 }
                }
            ]

            transitions: [
                Transition {
                    from: "GREEN"; to: "YELLOW"
                    NumberAnimation { targets: [greenLight1, yellowLight1]; properties: "opacity"; duration: 500 }
                },
                Transition {
                    from: "YELLOW"; to: "RED"
                    NumberAnimation { targets: [yellowLight1, redLight1]; properties: "opacity"; duration: 500 }
                },
                Transition {
                    from: "RED"; to: "GREEN"
                    NumberAnimation { targets: [redLight1, greenLight1]; properties: "opacity"; duration: 500 }
                }
            ]

            Timer {
                interval: 750
                running: visible; repeat: true
                onTriggered: {
                    switch(signalHousing1.state) {
                    case "GREEN":
                        signalHousing1.state = "YELLOW"
                        break
                    case "YELLOW":
                        signalHousing1.state = "RED"
                        break
                    case "RED":
                        signalHousing1.state = "GREEN"
                        break
                    }
                }
            }
        }

        Footnote {
            text: "http://doc.qt.nokia.com/4.7-snapshot/qdeclarativeanimation.html"
        }
    }

    Slide {
        id: scopeSlide
        title: "Scope"
        content: [
            "The properties of an element are visible to all elements in the same document",
            "Elements declared in seperate documents may only access the properties of each other's top most elements"
        ]

        contentWidth: width - scopeCS.width - contentPadding

        CodeSection {
            id: scopeCS
            text: "
<span style='color: green'>// Button.qml</span><br/>
<span style='color: purple'>Item</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: button<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: goldenrod'>signal</span> clicked<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: goldenrod'>property alias color</span>: rect.color<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: goldenrod'>property alias label</span>: label.text<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>width</span>: 300; <span style='color: red'>height</span>: 150<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Rectangle</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: rect<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>color</span>: <span style='color: green'>\"cyan\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>anchors.fill</span>: parent<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>radius</span>: 5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>Text</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>id</span>: label</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>text</span>: <span style='color: green'>\"Click me!\"</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>anchors.centerIn</span>: parent<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: purple'>MouseArea</span> {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>anchors.fill</span>: rect<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: red'>onClicked</span>: button.clicked()<br/>
&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
}"
        }
    }

    Slide {
        id: particles

        title: "Particles"

        Rectangle {
            anchors.fill: parent
            color: "black"
            MouseTrail {}
            Loader {
                id: flameLoader
                anchors.fill: parent
            }
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.RightButton
                onClicked: {
                    if(flameLoader.source != "")
                        flameLoader.source = ""
                    else
                        flameLoader.source = "Flames.qml"
                }
            }
        }
        onVisibleChanged: if(!visible) flameLoader.source = ""
    }

    Slide {
        title: "Acknowledgements"
        content: [
            " Jasmin Blanchette; Mark Summerfield. \"A Brief History of Qt\" in C++ GUI Programming with Qt 4, 2nd ed.",
            " http://doc.qt.nokia.com/4.7-snapshot/qdeclarativeanimation.html",
            " http://doc.qt.nokia.com/4.7-snapshot/qdeclarativestates.html",
            " http://qt-project.org/doc/qt-4.8/qml-anchor-layout.html",
            " http://doc.qt.nokia.com/4.7-snapshot/propertybinding.html",
            " http://qt-project.org/doc/qt-4.8/qdeclarativebasictypes.html"
        ]
    }

    Slide {
        centeredText: "Questions?"
    }
}
