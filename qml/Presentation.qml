/***************************************************************************
**
** This file is part of the QML Presentation System **
**
** Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).*
**
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**
**  * Redistributions of source code must retain the above copyright notice,
**    this list of conditions and the following disclaimer.
**  * Redistributions in binary form must reproduce the above copyright notice,
**    this list of conditions and the following disclaimer in the documentation
**    and/or other materials provided with ** the distribution.
**  * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the
**    names of its contributors may be used to endorse or promote products
**    derived from this software without specific ** prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
**************************************************************************/

import QtQuick 2.0
import Qt.labs.particles 2.0

Item {
    id: root

    property variant slides: []
    property int currentSlide;

    property bool faded: false

    property int userNum;

    Component.onCompleted: {
        var slideCount = 0;
        var slides = [];
        for (var i=0; i<root.resources.length; ++i) {
            var r = root.resources[i];
            if (r.isSlide) {
                slides.push(r);
            }
        }

        root.slides = slides;
        root.userNum = 0;

        // Make first slide visible...
        if (root.slides.length > 0) {
            root.currentSlide = 0;
            root.slides[root.currentSlide].visible = true;
        }
    }

    function switchSlides(from, to, forward) {
        from.visible = false
        to.visible = true
        return true
    }

    function goToNextSlide() {
        root.userNum = 0
        if (faded)
            return
        if (root.currentSlide + 1 < root.slides.length) {
            var from = slides[currentSlide]
            var to = slides[currentSlide + 1]
            if (switchSlides(from, to, true)) {
                currentSlide = currentSlide + 1;
                root.focus = true;
            }
        }
    }

    function goToPreviousSlide() {
        root.userNum = 0
        if (root.faded)
            return
        if (root.currentSlide - 1 >= 0) {
            var from = slides[currentSlide]
            var to = slides[currentSlide - 1]
           if (switchSlides(from, to, false)) {
                currentSlide = currentSlide - 1;
               root.focus = true;
           }
        }
    }

    function goToUserSlide() {
        --userNum;
        if (root.faded || userNum >= root.slides.length)
            return
        if (userNum < 0)
            goToNextSlide()
        else if (root.currentSlide != userNum) {
            var from = slides[currentSlide]
            var to = slides[userNum]
           if (switchSlides(from, to, userNum > currentSlide)) {
                currentSlide = userNum;
               root.focus = true;
           }
        }
    }

    focus: true

    Keys.onSpacePressed: goToNextSlide()
    Keys.onRightPressed: goToNextSlide()
    Keys.onDownPressed: goToNextSlide()
    Keys.onLeftPressed: goToPreviousSlide()
    Keys.onUpPressed: goToPreviousSlide()
    Keys.onEscapePressed: Qt.quit()
    Keys.onPressed: {
        if (event.key >= Qt.Key_0 && event.key <= Qt.Key_9)
            userNum = 10 * userNum + (event.key - Qt.Key_0)
        else {
            if (event.key == Qt.Key_Return || event.key == Qt.Key_Enter)
                goToUserSlide();
            else if (event.key == Qt.Key_Backspace)
                goToPreviousSlide();
            else if (event.key == Qt.Key_C)
                root.faded = !root.faded;
            userNum = 0;
        }
    }

    Rectangle {
        id: blackout
        z: 1000
        color: "black"
        anchors.fill: parent
        opacity: root.faded ? 1 : 0
        Behavior on opacity { NumberAnimation { duration: 250 } }
    }

    Rectangle {
        id: background
        anchors.fill: parent
        color: colorTimer.color
        Behavior on color {
            ColorAnimation { duration: 750 }
        }

        Image {
            anchors.fill: parent
            source: "content/gradient.png"
            fillMode: Image.TileHorizontally
        }

        Timer {
            id: colorTimer
            interval: 2000
            running: true; repeat: true
            property color color: colors[index]
            property int index: 0
            property variant colors: ["#afaffa", "#afafaf", "#9083f3", "steelblue"]
            onTriggered: {
                var tmp = (index + 1) % colors.length
                index = tmp

                interval = (Math.random() * 2000) + 2000
            }
        }
    }

    ParticleSystem { id: sys }
    ColoredParticle {
        system: sys
        id: cp
        image: "content/particle.png"
        colorVariation: 0.1
        color: "#fffaaffa"
        additive: 1
        z: blackout.opacity > 0 ? blackout.z + 1 : background.z
    }
    TrailEmitter {
        system: sys
        emitting: true
        anchors.fill: parent
        particlesPerSecond: 75
        particleDuration: 1000
        acceleration: AngleVector { angle: 0; angleVariation: 360; magnitude: 75; }
        particleSize: 4
        particleEndSize: 0
        particleSizeVariation: 2
    }

    ParticleSystem { id: burstSys }
    ColoredParticle {
        system: burstSys
        id: burstParticle
        image: "content/particle.png"
        colorVariation: 0.1
        color: "#fffffafa"
        additive: 1
        z: blackout.opacity > 0 ? blackout.z + 1 : background.z
    }
    TrailEmitter {
        id: burster
        system: burstSys
        x: mouseArea.mouseX; y: mouseArea.mouseY
        emitting: mouseArea.pressed
        particlesPerSecond: 1000
        particleDuration: transitionTime
        speed: AngleVector { angle: 0; angleVariation: 360; magnitude: 100 }
        particleSize: 4
        particleEndSize: 0
        particleSizeVariation: 2
    }
    Attractor {
        id: burstAttractor; x: mouseArea.mouseX; y: mouseArea.mouseY; strength: 2000;
        system: burstSys
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button == Qt.RightButton)
                goToPreviousSlide()
            else
                goToNextSlide()
            burster.burst(0.1)
        }
    }
}
